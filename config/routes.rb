Rails.application.routes.draw do
  post '/rate' => 'rater#create', :as => 'rate'
  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  namespace :api do
    namespace :v1 do
      resources :sessions, only: [:create] do
        collection do
          get :me
          post :social_login
        end
      end

      resources :users, only: [:create]
      resources :movies, only: [:index]
    end
  end

  root to: "home#spa"
  get "*all", to: "home#spa"
end
