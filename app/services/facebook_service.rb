require "koala"

class FacebookService
  class << self
    def get_profile(token)
      graph = Koala::Facebook::API.new(token)
      graph.get_object("me", {fields: "email, name"})
    end

    def get_new_access_token(token)
      oauth = Koala::Facebook::OAuth.new(ENV["FACEBOOK_APP_ID"], ENV["FACEBOOK_SECRET"])
      h = oauth.exchange_access_token_info(token)

      h.try(:[], "access_token")
    end
  end
end
