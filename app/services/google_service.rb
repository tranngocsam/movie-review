require "httparty"

class GoogleService
  class << self
    def user_profile(token)
      begin
        response = HTTParty.get("https://www.googleapis.com/oauth2/v2/userinfo",
          headers: {
            'Authorization' => "OAuth #{token}"
          }
        )

        JSON.parse(response.body)
      rescue StandardError => e
        report "Cannot get user info: #{e.message}"
      end
    end
  end
end
