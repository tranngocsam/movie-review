class RegisterForm
  extend ActiveModel::Naming
  include ActiveModel::Conversion
  include ActiveModel::Validations
  include ActiveModel::AttributeAssignment

  attr_accessor :user
  attr_accessor :first_name, :last_name, :email, :password

  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :password, presence: true
  validates :email, presence: true, email: true
  validate :require_unique_email

  validates_length_of :password, minimum: 8

  def save
    if self.valid?
      self.user = User.create(attrs)
      self.user.persisted?
    else
      false
    end
  end

  private

  def require_unique_email
    if self.email.present?
      id_object = User.find_by_email(self.email)

      if id_object.present? && (self.user.blank? || id_object != self.user)
        self.errors.add(:email, "is already registered")
      end
    end
  end

  def attrs
    {
      first_name: self.first_name,
      last_name: self.last_name,
      email: self.email,
      password: self.password
    }
  end
end
