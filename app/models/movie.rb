class Movie < ApplicationRecord
  enum status: %w(Pending Out Discontinued)
end
