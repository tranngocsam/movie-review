require "koala"

class SocialConnection < ApplicationRecord

  belongs_to :social_connectable, polymorphic: true

  validates_presence_of :social_connectable
  validates_uniqueness_of :uid, scope: :social_connectable_id

  after_initialize :set_default_values

  private

  def set_default_values
    self.credentials ||= {}
    self.raw ||= {}
    self.external_info ||= {}
  end
end
