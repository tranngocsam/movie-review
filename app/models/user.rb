class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :social_connections, as: :social_connectable, dependent: :destroy

  def self.find_by_email(email)
    self.where("LOWER(email) = ?", email.try(:downcase)).first
  end

  def self.from_omniauth(auth)
    user = auth['uid'] && User.joins(:social_connections).where('social_connections.provider': auth['provider'], 'social_connections.uid': auth['uid']).first
    email = auth['info']['email']

    if user.nil? && email
      user = User.find_by_email(email)
      user.social_connections.build(provider: auth['provider'], uid: auth['uid'], social_connectable: user) unless user.nil?
    end

    if user.nil?
      user = self.build_with_omniauth(auth)
    end

    user
  end

  def self.build_with_omniauth(auth)
    new do |user|
      info = auth['info']

      user.social_connections.build(provider: auth['provider'], uid: auth['uid'], social_connectable: user)

      name        = info['name']
      first_name  = info['first_name']
      last_name   = info['last_name']

      if first_name.blank? && last_name.blank? && name.present?
        tmp = name.split(/\s+/)
        first_name = tmp.shift
        last_name = tmp.pop
      end

      user.first_name  = first_name || '' if %w(facebook google_login).include?(auth['provider'])
      user.last_name   = last_name || '' if %w(facebook google_login).include?(auth['provider'])
      user.email       = info['email'] || '' if %w(facebook google_login).include?(auth['provider'])
      user.password = SecureRandom.hex(8)
    end
  end
end
