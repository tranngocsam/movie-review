class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  private

  # is_new_user - Reserved for other purpose like sending email,...
  def log_user_in(user, is_new_user = false)
    sign_in(:user, user)
  end
end
