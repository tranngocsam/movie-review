class Api::V1::MoviesController < Api::V1::BaseController
  before_action :require_user

  def index
    movies = Movie.all.paginate(page: parsed_params[:page], per_page: parsed_params[:per_page])
    respond_json_results(movies)
  end

  private

  def register_params
    params.require(:user).permit(:first_name, :last_name, :email, :password)
  end
end
