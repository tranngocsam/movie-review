class Api::V1::UsersController < Api::V1::BaseController

  def create
    register_form = RegisterForm.new
    register_form.assign_attributes(register_params)

    if register_form.save
      user = register_form.user
      log_user_in(user, true)

      respond_json_results(as_json_for_current_user)
    else
      respond_json_errors(errors: register_form.errors)
    end
  end

  private

  def register_params
    params.require(:user).permit(:first_name, :last_name, :email, :password)
  end
end
