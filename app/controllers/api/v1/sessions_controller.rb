class Api::V1::SessionsController < Api::V1::BaseController
  def me
    respond_json_results(as_json_for_current_user)
  end

  def create
    user = User.find_by_email(login_params[:login])

    if user.present? && user.valid_password?(login_params[:password])
      log_user_in(user)
      respond_json_results(as_json_for_current_user)
    else
      respond_json_errors(message: "invalid login or password", status: 422)
    end
  end

  # With social login, we will get information using token, then create user if user haven't registered
  # TODO: Save token
  def social_login
    token = social_params[:token]

    case social_params[:provider].try(:downcase)
    when "facebook"
      profile = FacebookService.get_profile(token)
      omniauth = {"provider" => "facebook", "uid" => profile["id"]}
      omniauth["info"] = profile
    when "google_login"
      profile = GoogleService.user_profile(token)

      omniauth = {"provider" => "google_login", "uid" => profile["id"]}
      omniauth["info"] = profile
      omniauth["info"]["image"] = profile["picture"]
    else
      respond_json_errors(message: "unsupport provider")
      return
    end

    user = User.from_omniauth(omniauth)
    is_new_user = user.new_record?

    if is_new_user
      unless user.save
        respond_json_errors(errors: user.errors)
        return
      end
    end

    log_user_in(user, is_new_user)
    respond_json_results(as_json_for_current_user)
  end

  private

  def login_params
    params.require(:user).permit(:login, :password)
  end

  def social_params
    params.require(:social).permit(:provider, :token)
  end
end
