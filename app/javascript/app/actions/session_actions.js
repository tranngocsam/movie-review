import _ from "lodash";
import constants from "../constants";
import { setRequestStatus, performAction } from "./utils";
import {
  doLoadCurrentUser,
  doRegister,
  doLogin,
} from "../apis/session";

export function loadCurrentUser(params) {
  var requestType = constants.SESSION.LOAD_CURRENT_USER;

  return performAction(requestType, ()=>
    doLoadCurrentUser(params)
  );
}

export function login(login, password, params) {
  var requestType = constants.SESSION.LOGIN;
  var submittedParams = _.assign({}, params || {});

  submittedParams.user = { login: login, password: password };

  return performAction(requestType, ()=>
    doLogin(submittedParams)
  );
}

export function register(user, params) {
  var requestType = constants.SESSION.REGISTER;
  var submittedParams = _.assign({}, params || {});

  submittedParams.user = user;

  return performAction(requestType, ()=>
    doRegister(submittedParams)
  );
}
