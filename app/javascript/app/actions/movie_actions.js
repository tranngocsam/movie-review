import _ from "lodash";
import constants from "../constants";
import { setRequestStatus, performAction } from "./utils";
import {
  doLoadMovies
} from "../apis/movie";

export function loadMovies(params) {
  var requestType = constants.MOVIE.LOAD_MOVIES;

  return performAction(requestType, ()=>
    doLoadMovies(params)
  );
}
