import React from 'react';
import {connect} from "react-redux";
import classNames from "classnames";
import _ from "lodash";
import MoviesList from "./movies/movies_list";
import AppHeader from "./layout/app_header";
import PageFooter from "../components/layout/page_footer";
import {navigateTo} from "../utils/misc";
import constants from '../constants';

class Movie extends React.Component {
  constructor(props) {
    super(props);
    this.state = this.getInitialState();
  }

  getInitialState() {
    return {};
  }

  componentWillReceiveProps(newProps) {
    var currentUserActionStatus = newProps.currentUserActionStatus;
    var oldActionStatus = this.props.currentUserActionStatus;
    var currentUser = newProps.currentUser;

    // Has loaded current user info
    if (currentUserActionStatus && currentUserActionStatus == constants.REQUEST.LOADING_SUCCESS && currentUserActionStatus != oldActionStatus) {
      // Redirect to sign in page
      if (!currentUser) {
        navigateTo("/signin");
      }
    }
  }

  render() {
    var moviesList;
    if (this.props.currentUser) {
      moviesList = (<MoviesList />);
    } else {
      moviesList = (
        <div>Loading</div>
      );
    }
    return (
      <div className="page client-page">
        <AppHeader />

        {moviesList}

        <PageFooter />
      </div>
    )
  }
}

function stateToProps(state, ownProps) {
  let sessionStore = state.sessionStore;
  let props = {};

  if (sessionStore) {
    props.currentUser = sessionStore.currentUser;

    if (sessionStore.actionType == constants.SESSION.LOAD_CURRENT_USER) {
      props.currentUserActionStatus = sessionStore.actionStatus;
    }
  }

  return props;
}

Movie = connect(stateToProps, {
})(Movie);

export default Movie;
