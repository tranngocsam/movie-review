import React from 'react';
import {connect} from "react-redux";
import classNames from "classnames";
import _ from "lodash";
import SigninFormView from "../components/session/signin_form_view";
import PageHeader from "../components/layout/page_header";
import PageFooter from "../components/layout/page_footer";
import {navigateTo} from "../utils/misc";
import constants from '../constants';

class Signin extends React.Component {
  constructor(props) {
    super(props);
    this.state = this.getInitialState();
  }

  getInitialState() {
    return {};
  }

  componentWillReceiveProps(newProps) {
    var currentUser = newProps.currentUser;
    var oldCurrentUser = this.props.currentUser;

    // Redirect when user logs in
    if (currentUser && (!oldCurrentUser || oldCurrentUser.id != currentUser.id)) {
      var urlParams = newProps.location.query;
      var backUrl = urlParams.redirect_to || urlParams.back || "/movies";

      navigateTo(backUrl);
    }
  }

  render() {
    return (
      <div className="page page-signin client-page">
        <PageHeader />

        <SigninFormView />

        <PageFooter/>
      </div>
    )
  }
}

function stateToProps(state, ownProps) {
  let sessionStore = state.sessionStore;
  let props = {};

  if (sessionStore) {
    props.currentUser = sessionStore.currentUser;
  }

  return props;
}

Signin = connect(stateToProps, {
})(Signin);

export default Signin;
