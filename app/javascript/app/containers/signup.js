import React from 'react';
import {connect} from "react-redux";
import classNames from "classnames";
import _ from "lodash";
import SignupFormView from "../components/session/signup_form_view";
import PageHeader from "../components/layout/page_header";
import PageFooter from "../components/layout/page_footer";
import {navigateTo} from "../utils/misc";
import constants from '../constants';

class Signup extends React.Component {
  constructor(props) {
    super(props);
    this.state = this.getInitialState();
  }

  getInitialState() {
    return {};
  }

  componentWillReceiveProps(newProps) {
    var currentUser = newProps.currentUser;
    var oldCurrentUser = this.props.currentUser;

    // Redirect after sign up
    if (currentUser && (!oldCurrentUser || oldCurrentUser.id != currentUser.id)) {
      var urlParams = newProps.location.query;
      var backUrl = urlParams.redirect_to || urlParams.back || "/movies";

      navigateTo(backUrl);
    }
  }

  render() {
    return (
      <div className="page page-signup client-page">
        <PageHeader />
        <SignupFormView />
        <PageFooter/>
      </div>
    )
  }
}

function stateToProps(state, ownProps) {
  let sessionStore = state.sessionStore;
  let props = {};

  if (sessionStore) {
    props.currentUser = sessionStore.currentUser;
  }

  return props;
}

Signup = connect(stateToProps, {
})(Signup);

export default Signup;
