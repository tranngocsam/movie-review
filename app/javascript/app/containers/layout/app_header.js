import React from 'react';
import {connect} from "react-redux";
import _ from "lodash";
import PageHeader from "../../components/layout/page_header";
import constants from "../../constants";

class AppHeader extends React.Component {
  constructor(props) {
    super(props);
    this.state = this.getInitialState();
  }

  getInitialState() {
    return {};
  }

  render() {
    var currentUser = this.props.currentUser;

    return (
      <PageHeader currentUser={this.props.currentUser} />
    );
  }
}

function stateToProps(state, ownProps) {
  let sessionStore = state.sessionStore;
  let props = {};

  if (sessionStore) {
    props.currentUser = sessionStore.currentUser;
  }

  return props;
}

AppHeader = connect(stateToProps, {
})(AppHeader);

export default AppHeader;
