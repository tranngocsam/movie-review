import React from 'react';
import {connect} from "react-redux";
import classNames from "classnames";
import _ from "lodash";
import AppHeader from "./layout/app_header";
import PageFooter from "../components/layout/page_footer";
import {navigateTo} from "../utils/misc";
import constants from '../constants';

class Homepage extends React.Component {
  constructor(props) {
    super(props);
    this.state = this.getInitialState();
  }

  getInitialState() {
    return {};
  }

  componentWillReceiveProps(newProps) {
    var currentUser = newProps.currentUser;
    var oldUser = this.props.currentUser;

    // Redirect to /movies page if user is logged in
    if (currentUser && (!oldUser || oldUser.id != currentUser)) {
      navigateTo("/movies");
    }
  }

  render() {
    return (
      <div className="page client-page">
        <AppHeader />

        <section className='section'>
          <div className='container'>
            This is the landing page
          </div>
        </section>

        <PageFooter />
      </div>
    )
  }
}

function stateToProps(state, ownProps) {
  let sessionStore = state.sessionStore;
  let props = {};

  if (sessionStore) {
    props.currentUser = sessionStore.currentUser;
  }

  return props;
}

Homepage = connect(stateToProps, {
})(Homepage);

export default Homepage;
