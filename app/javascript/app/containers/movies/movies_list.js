import React from 'react';
import {connect} from "react-redux";
import classNames from "classnames";
import _ from "lodash";
import MoviesListView from "../../components/movies/movies_list_view";
import {loadMovies} from "../../actions/movie_actions";
import {navigateTo} from "../../utils/misc";
import constants from '../../constants';

class MoviesList extends React.Component {
  constructor(props) {
    super(props);
    this.state = this.getInitialState();
    _.bindAll(this, "loadMovies");
  }

  getInitialState() {
    return {};
  }

  componentWillMount() {
    this.loadMovies();
  }

  loadMovies(page, perPage) {
    var params = {};
    params.page = page || 1;
    params.per_page = perPage || 20;

    this.props.loadMovies(params);
  }

  render() {
    return (
      <MoviesListView movies={this.props.movies}
                      moviesPaginationInfo={this.props.moviesPaginationInfo}
                      goToPage={this.loadMovies} />
    )
  }
}

function stateToProps(state, ownProps) {
  let movieStore = state.movieStore;
  let props = {};

  if (movieStore) {
    props.movies = movieStore.movies;
    props.moviesPaginationInfo = movieStore.moviesPaginationInfo;
  }

  return props;
}

MoviesList = connect(stateToProps, {
  loadMovies
})(MoviesList);

export default MoviesList;
