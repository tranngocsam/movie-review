import React from "react";
import {connect} from "react-redux";
import PasswordSigninView from "../../components/session/password_signin_view";
import {login} from "../../actions/session_actions";
import constants from "../../constants";

class PasswordSignin extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return <PasswordSigninView login={this.props.login}
                               loginActionStatus={this.props.loginActionStatus}
                               loginResponseError={this.props.loginResponseError}
                               switchToSignup={this.props.switchToSignup} />
  }
}

function stateToProps(state, ownProps) {
  let sessionStore = state.sessionStore;
  let props = {};

  if (sessionStore) {
    if (sessionStore.actionType == constants.SESSION.LOGIN) {
      props.loginActionStatus = sessionStore.actionStatus;
      props.loginResponseError = sessionStore.responseError;
    }
  }

  return props;
}

PasswordSignin = connect(stateToProps, {
  login
})(PasswordSignin);

export default PasswordSignin;
