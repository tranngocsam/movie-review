import React from "react";
import {connect} from "react-redux";
import PasswordSignupView from "../../components/session/password_signup_view";
import {register} from "../../actions/session_actions";
import constants from "../../constants";

class PasswordSignup extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return <PasswordSignupView signup={this.props.register}
                               signupActionStatus={this.props.signupActionStatus}
                               signupResponseError={this.props.signupResponseError}
                               switchToLogin={this.props.switchToLogin} />
  }
}

function stateToProps(state, ownProps) {
  let sessionStore = state.sessionStore;
  let props = {};

  if (sessionStore) {
    if (sessionStore.actionType == constants.SESSION.REGISTER) {
      props.signupActionStatus = sessionStore.actionStatus;
      props.signupResponseError = sessionStore.responseError;
    }
  }

  return props;
}

PasswordSignup = connect(stateToProps, {
  register
})(PasswordSignup);

export default PasswordSignup;
