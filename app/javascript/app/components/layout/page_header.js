import React from "react";
import classNames from "classnames";
import _ from "lodash";
import { navigateTo } from "../../utils/misc";
import constants from "../../constants";

class PageHeader extends React.Component {
  constructor(props) {
    super(props);
    this.state = this.getInitialState();
  }

  getInitialState() {
    return {};
  }

  componentWillMount() {
  }

  componentWillUnmount() {
  }

  render() {
    var currentUser = this.props.currentUser;
    var lis = [];

    if (currentUser) {
      lis.push(
        <li key="signout"><a href="/sign_out">Sign out</a></li>
      );
    } else {
      lis.push(
        <li key="signin"><a href="/signin" onClick={navigateTo}>Sign in</a></li>
      );
      lis.push(
        <li key="signup"><a href="/signup" onClick={navigateTo}>Sign up</a></li>
      );
    }

    return (
      <div>
        <div className="text-right">
          <ul className="list-inline">
            {lis}
          </ul>
        </div>
      </div>
    );
  }
}

export default PageHeader;
