import React from "react";
import classNames from "classnames";
import _ from "lodash";
import {
  isEmailValid,
  isPasswordValid,
  navigateTo
} from "../../utils/misc";
import constants from "../../constants";

class PasswordSignupView extends React.Component {
  constructor(props) {
    super(props);
    this.state = this.getInitialState();
    _.bindAll(this, "signup");
  }

  getInitialState() {
    return {};
  }

  componentWillReceiveProps(newProps) {
    if (
      newProps.signupResponseError &&
      newProps.signupActionStatus == constants.REQUEST.LOADING_ERROR
    ) {
      this.setState({ clearSignupErrors: false });
    }

    if (
      newProps.signupResponseError &&
      (!this.props.signupResponseError ||
        !_.isEqual(
          newProps.signupResponseError,
          this.props.signupResponseError
        ))
    ) {
      this.setState({ signupResponseError: newProps.signupResponseError });
    }
  }

  switchToLogin(event) {
    event.preventDefault();
    navigateTo("/signin");
  }

  setFormValueFromEvent(field, event) {
    var value = event.target.value;
    var form = this.state.form || {};

    form[field] = value;

    var states = { form: form };
    var signupResponseError = this.state.signupResponseError || {};
    var errors = signupResponseError.errors || {};

    if (errors.zip_code && field == "zip_code") {
      delete signupResponseError.errors.zip_code;
      states.signupResponseError = signupResponseError;
    } else if (errors.email && field == "email") {
      delete signupResponseError.errors.email;
      states.signupResponseError = signupResponseError;
    }

    this.setState(states);
  }

  getSignupFormValues() {
    return this.state.form || {};
  }

  getSignupFormErrors() {
    var result = this.getSignupFormValues();
    var errors = {};
    if (!result.first_name) {
      errors.first_name = "can't be blank";
    }

    if (!result.last_name) {
      errors.last_name = "can't be blank";
    }

    if (!result.email) {
      errors.email = "can't be blank";
    } else if (!isEmailValid(result.email)) {
      errors.email = "is not valid";
    }

    if (!result.password) {
      errors.password = "can't be blank";
    } else if (!isPasswordValid(result.password)) {
      errors.password = "must be at least 5 characters";
    }

    return errors;
  }

  signup(event) {
    event.preventDefault();
    var errors = this.getSignupFormErrors();

    if (_.keys(errors).length == 0) {
      this.props.signup(this.getSignupFormValues());
      this.setState({ submittedSignupForm: false, clearSignupErrors: true });
    } else {
      this.setState({ submittedSignupForm: true });
    }
  }

  renderErrorOf(errors, field) {
    if (!errors || !field || !errors[field]) {
      return;
    }

    return <p className="help-block error-block">{errors[field]}</p>;
  }

  render() {
    var submitButton;
    var submittedSignupForm = this.state.submittedSignupForm;
    var signupFormErrors = {};

    if (!this.state.clearSignupErrors) {
      signupFormErrors = (this.state.signupResponseError || {}).errors || {};
    }

    if (submittedSignupForm) {
      signupFormErrors = _.assign(
        {},
        signupFormErrors,
        this.getSignupFormErrors()
      );
    }

    if (this.props.signupActionStatus == constants.REQUEST.LOADING) {
      submitButton = <img src="/assets/ajax-loader.gif" alt="Submitting" />;
    } else {
      submitButton = (
        <button
          className="btn btn-primary"
          type="submit"
        >
          Sign up
        </button>
      );
    }

    return (
      <form
        className="form-horizontal"
        onSubmit={this.signup}
        noValidate
      >
        <div className="name-inputs-group">
          <div
            className={classNames({
              "form-group": true,
              error: signupFormErrors.first_name
            })}
          >
            <label className="col-sm-2">First Name</label>
            <div className="col-sm-10">
              <input
                className="inp--default"
                ref="signupFirstName"
                name="first_name"
                placeholder="First"
                type="text"
                onChange={evt => this.setFormValueFromEvent("first_name", evt)}
              />
              {this.renderErrorOf(signupFormErrors, "first_name")}
            </div>
          </div>
          <div
            className={classNames({
              "form-group": true,
              error: signupFormErrors.last_name
            })}
          >
            <label className="col-sm-2">Last Name</label>
            <div className="col-sm-10">
              <input
                className="inp--default"
                ref="signupLastName"
                name="last_name"
                placeholder="Last"
                type="text"
                onChange={evt => this.setFormValueFromEvent("last_name", evt)}
              />
              {this.renderErrorOf(signupFormErrors, "last_name")}
            </div>
          </div>
        </div>
        <div
          className={classNames({
            "form-group": true,
            error: signupFormErrors.email
          })}
        >
          <label className="col-sm-2" htmlFor="identity_form_email">
            Email
          </label>
          <div className="col-sm-10">
            <input
              className="inp--default"
              ref="signupEmail"
              name="email"
              placeholder="myemail@example.com"
              type="email"
              onChange={evt => this.setFormValueFromEvent("email", evt)}
            />
            {this.renderErrorOf(signupFormErrors, "email")}
          </div>
        </div>
        <div
          className={classNames({
            "form-group": true,
            error: signupFormErrors.password
          })}
        >
          <label
            className="col-sm-2"
            htmlFor="identity_form_password"
          >
            Password
          </label>
          <div className="col-sm-10">
            <input
              className="inp--default"
              ref="signupPassword"
              name="password"
              required="required"
              aria-required="true"
              type="password"
              onChange={evt => this.setFormValueFromEvent("password", evt)}
            />
            {this.renderErrorOf(signupFormErrors, "password")}
          </div>
        </div>
        <div className="form-group">
          <a
            className="text--light link--up js-show-login"
            href="#"
            onClick={this.switchToLogin}
          >
            I already have an account
          </a>
        </div>

        <div className="form-group">
          <div
            className="js-mouseflow-tag btn__container form__submit"
            data-mouseflow={JSON.stringify({ tags: "registration-attempt" })}
          >
            {submitButton}
          </div>
        </div>
      </form>
    );
  }
}

export default PasswordSignupView;
