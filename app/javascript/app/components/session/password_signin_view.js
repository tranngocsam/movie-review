import React from "react";
import classNames from "classnames";
import _ from "lodash";
import { navigateTo, isEmptyObject, isEmailValid } from "../../utils/misc";
import constants from "../../constants";

class PasswordSigninView extends React.Component {
  constructor(props) {
    super(props);
    this.state = this.getInitialState();
    _.bindAll(this, "signin");
  }

  getInitialState() {
    return {};
  }

  componentWillReceiveProps(newProps) {
    if (
      newProps.loginResponseError &&
      newProps.loginResponseError.message &&
      newProps.loginActionStatus == constants.REQUEST.LOADING_ERROR
    ) {
      alert(newProps.loginResponseError.message);
    }
  }

  switchToSignup(event) {
    event.preventDefault();
    navigateTo("/signup");
  }

  setFormValue(field, value) {
    this.setState({ field: value });
  }

  signin(event) {
    event.preventDefault();
    var form = this.getFormValues();

    if (isEmptyObject(this.getFormErrors(form))) {
      this.props.login(form.login, form.password);
      this.setState({ submitted: false });
    } else {
      this.setState({ submitted: true });
    }
  }

  getFormValues() {
    if (!this.refs.login || !this.refs.password) {
      return {};
    }

    var login = this.refs.login.value;
    var password = this.refs.password.value;

    return { login: login, password: password };
  }

  getFormErrors(form) {
    var errors = {};

    if (!form.login) {
      errors.login = "can't be blank";
    } else if (!isEmailValid(form.login)) {
      errors.login = "is invalid";
    }

    if (!form.password) {
      errors.password = "can't be blank";
    }

    return errors;
  }

  renderError(errors, field) {
    if (!errors || !field || !errors[field]) {
      return;
    }

    return <p className="help-block error-block mb0">{errors[field]}</p>;
  }

  render() {
    var submitButton;
    var form = this.getFormValues();
    var errors = (this.state.submitted && this.getFormErrors(form)) || {};

    if (this.props.loginActionStatus == constants.REQUEST.LOADING) {
      submitButton = <img src="/assets/ajax-loader.gif" alt="Submitting" />;
    } else {
      submitButton = (
        <button
          className="btn btn-primary"
          type="submit"
        >
          Sign in
        </button>
      );
    }

    return (
      <form className="form-horizontal" onSubmit={this.signin}>
        <div className={classNames({ "form-group": true, error: errors.login })}>
          <label className="col-sm-2" htmlFor="auth_key">
            Email address:
          </label>
          <div className="col-sm-10">
            <input
              name="auth_key"
              id="auth_key"
              className="inp--default"
              type="text"
              ref="login"
              onChange={evt => this.setFormValue("login", evt.target.value)}
            />
            {this.renderError(errors, "login")}
          </div>
        </div>
        <div
          className={classNames({ "form-group": true, error: errors.password })}
        >
          <label className="col-sm-2" htmlFor="password">
            Password:
          </label>
          <div className="col-sm-10">
            <input
              name="password"
              id="password"
              className="inp--default"
              type="password"
              ref="password"
              onChange={evt => this.setFormValue("password", evt.target.value)}
            />
            {this.renderError(errors, "password")}
          </div>
        </div>
        <div className="form__row">
          <a
            className="form--link text--light link--up"
            href="/password/new"
            onClick={navigateTo}
          >
            I forgot my password
          </a>
          <br />
          <a
            className="form--link text--light link--up js-show-registration"
            href="#"
            onClick={this.switchToSignup}
          >
            I don't have an account
          </a>
        </div>
        <div className="form__row">
          <div className="btn__container form__submit">{submitButton}</div>
        </div>
      </form>
    );
  }
}

export default PasswordSigninView;
