import React from "react";
import classNames from "classnames";
import _ from "lodash";
import PasswordSignup from "../../containers/session/password_signup";
import { navigateTo } from "../../utils/misc";

class SignupFormView extends React.Component {
  constructor(props) {
    super(props);
    this.state = this.getInitialState();
  }

  getInitialState() {
    return {};
  }

  render() {
    return (
      <section className="signup-form-view">
        <div className="container container--space">
          <div className="grid grid--center">
            <div className="grid__item lg--three-quarters">
              <h2 className="headline--big headline--clear-space headline--center">
                Registration
              </h2>
              <div>
                <PasswordSignup />
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default SignupFormView;
