import React from "react";
import classNames from "classnames";
import _ from "lodash";
import PasswordSignin from "../../containers/session/password_signin";

class SigninFormView extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <section>
        <div className="container container--space">
          <div className="grid grid--center">
            <div className="grid__item lg--three-quarters">
              <h2 className="headline--big headline--clear-space headline--center">Log in</h2>
              <p className="text--middle text--center text--short">Hey, welcome back!</p>
              <div className="grid">
                <div className="grid__item md--one-half separator--or">
                  <PasswordSignin />
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default SigninFormView;
