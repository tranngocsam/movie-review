import React from "react";
import classNames from "classnames";
import _ from "lodash";
import CustomPagination from "../commons/custom_pagination";
import {formatDate} from "../../utils/misc";
import constants from "../../constants";

class MoviesListView extends React.Component {
  constructor(props) {
    super(props);
    this.state = this.getInitialState();
  }

  getInitialState() {
    return {};
  }

  render() {
    var movies = this.props.movies || [];
    var moviesPaginationInfo = this.props.moviesPaginationInfo;
    var displayedMovies = [];
    var customPagination;

    if (moviesPaginationInfo) {
      customPagination = (
        <CustomPagination goToPage={this.props.goToPage}
                          totalPages={moviesPaginationInfo.total_pages}
                          currentPage={moviesPaginationInfo.current_page}
                          maxNumberOfLinks={5} />
      );
    }

    for (let i = 0; i < movies.length; i++) {
      let movie = movies[i];
      displayedMovies.push(
        <tr key={i}>
          <td>{movie.title}</td>
          <td>{formatDate(movie.released_date)}</td>
        </tr>
      );
    }

    if (movies.length == 0) {
      displayedMovies.push(
        <tr key="no_results">
          <td colSpan={2}>No movies found</td>
        </tr>
      );
    }

    return (
      <div className="movies-list">
        <table className="table table-strip">
          <thead>
            <tr>
              <th>Title</th>
              <th>Released date</th>
            </tr>
          </thead>
          <tbody>
            {displayedMovies}
          </tbody>
        </table>
        {customPagination}
      </div>
    );
  }
}

export default MoviesListView;
