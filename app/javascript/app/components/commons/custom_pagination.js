import React from "react";
import _ from "lodash";

class CustomPagination extends React.Component {
  constructor(props) {
    super(props);
    _.bindAll(this, "goToPage");
  }

  goToPage(pageNumber, event) {
    if (event) {
      event.preventDefault();
    }

    this.props.goToPage(pageNumber);
  }

  render() {
    var pages = [];
    var currentPage = this.props.currentPage;
    var totalPages = this.props.totalPages;
    var hasMore = this.props.hasMore;
    var maxNumberOfLinks = this.props.maxNumberOfLinks;
    var startLeft = currentPage - Math.ceil((maxNumberOfLinks - 3)/2);
    var endRight = currentPage + Math.ceil((maxNumberOfLinks - 3)/2);

    startLeft = startLeft <= 2 ? 2 : startLeft;
    endRight = endRight >= totalPages - 1 ? totalPages - 1 : endRight;

    if (totalPages > 1 || hasMore) {
      if (currentPage == 1) {
        pages.push(
          <span className="btn btn--link btn-page active" key="page_1">1</span>
        );
      } else {
        pages.push(
          <a href="#" className="btn btn--link btn-page" key="previous_page" onClick={(evt) => this.goToPage(currentPage - 1, evt)}>Previous Page</a>
        );

        pages.push(
          <a href="#" className="btn btn--link btn-page" key="page_1" onClick={(evt) => this.goToPage(1, evt)}>1</a>
        );
      }

      if (startLeft > 2) {
        pages.push(
          <span className="btn btn--link three-dots btn-page" key="left_3_dots">...</span>
        );
      }

      for (let i = startLeft; i < currentPage; i++) {
        pages.push(
          <a href="#" className="btn btn--link btn-page" key={"page_" + i} onClick={(evt) => this.goToPage(i, evt)}>{i}</a>
        );
      }

      if (currentPage != 1) {
        pages.push(
          <span className="btn btn--link btn-page active" key={"page_" + currentPage}>{currentPage}</span>
        );
      }

      for (let i = currentPage + 1; i <= endRight; i++) {
        pages.push(
          <a href="#" className="btn btn--link btn-page" key={"page_" + i} onClick={(evt) => this.goToPage(i, evt)}>{i}</a>
        );
      }

      if (endRight < totalPages - 1) {
        pages.push(
          <span className="btn btn--link three-dots btn-page" key="right_3_dots">...</span>
        );
      }

      if (currentPage != totalPages) {
        pages.push(
          <a href="#" className="btn btn--link btn-page" key={"page_" + totalPages} onClick={(evt) => this.goToPage(totalPages, evt)}>{totalPages}</a>
        );
      }

      if (hasMore) {
        pages.push(<span className="btn btn--link three-dots btn-page" key="last_3_dots">...</span>);
      }

      if (currentPage < totalPages || hasMore) {
        pages.push(
          <a href="#" className="btn btn--link btn-page" key="next_page" onClick={(evt) => this.goToPage(currentPage + 1, evt)}>Next Page</a>
        );
      }
    }

    return (
      <div className="custom-pagination">
        {pages}
      </div>
    );
  }
}

export default CustomPagination
