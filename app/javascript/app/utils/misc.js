import { browserHistory } from "react-router";
import _ from "lodash";
import moment from "moment";

export function isEmptyObject(object) {
  return jQuery.isEmptyObject(object);
}

export function isEmailValid(email) {
  if (!email) {
    return false;
  }

  var $emailTag = jQuery("<input type='email' value='" + email + "' />");
  return $emailTag[0].checkValidity();
}

export function isPasswordValid(password) {
  return password && password.length >= 8;
}

export function navigateTo(pathOrEvent) {
  var path;
  if (typeof(pathOrEvent) == "object") {
    path = jQuery(pathOrEvent.currentTarget).attr("href");
    pathOrEvent.preventDefault();
  } else {
    path = pathOrEvent;
  }

  browserHistory.push(path);
  scrollToTop();
}

export function getMessageFromResponseError(responseError) {
  if (!responseError) {
    return "";
  }

  var message = responseError.message || responseError.error || "";
  if (!message) {
    var errors = responseError.errors;
    if (errors) {
      var tmp = [];
      for (let p in errors) {
        tmp.push(p + ": " + errors[p].join(", "));
      }
      message = tmp.join("; ");
    } else {
      message = "Unknown error";
    }
  }

  return message;
}

export function scrollToTop(speed) {
  jQuery('html, body').animate({ scrollTop: 0 }, speed || "slow");
}

export function formatDate(date, format) {
  format = format || "MM/DD/YYYY";
  return date ? moment(date).format(format) : date;
}
