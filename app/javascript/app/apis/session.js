import { submitRequest } from "./utils";

export function doLoadCurrentUser(params) {
  return submitRequest("/api/v1/sessions/me", "get", params);
}

export function doLogin(params) {
  return submitRequest("/api/v1/sessions", "post", params);
}

export function doRegister(params) {
  return submitRequest("/api/v1/users", "post", params);
}
