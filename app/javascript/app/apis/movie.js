import { submitRequest } from "./utils";

export function doLoadMovies(params) {
  return submitRequest("/api/v1/movies", "get", params);
}
