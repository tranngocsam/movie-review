import constants from '../constants';
import _ from "lodash";
import { handleLoadedData } from "./utils";

export default function movieReducer(state = {}, action) {
  const {type, status} = action;

  switch (type) {
    case constants.MOVIE.LOAD_MOVIES:
      var options = {};

      if (status == constants.REQUEST.LOADING_SUCCESS) {
        let movies = (action.responseData || {}).data;
        let pagination = (action.responseData || {}).pagination;

        options.successData = {movies: movies, moviesPaginationInfo: pagination};
      }

      return handleLoadedData(type, status, state, action, options);
    default:
      return state;
  }
}
