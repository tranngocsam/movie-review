import { combineReducers } from 'redux';
import sessionReducer from './session_reducer';
import movieReducer from "./movie_reducer";

const reducer = combineReducers({
  sessionStore: sessionReducer,
  movieStore: movieReducer
});

export default reducer;
