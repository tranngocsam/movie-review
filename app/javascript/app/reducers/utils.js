import _ from "lodash";
import actionTypes from '../constants';

export function handleLoadedData(type, status, state, action, options) {
  options = options || {};

  switch(status) {
    case actionTypes.REQUEST.LOADING:
      var requestData = _.assign({
        actionType: type,
        actionStatus: actionTypes.REQUEST.LOADING
      }, options.requestData);

      return _.assign(_.cloneDeep(state), requestData);
    case actionTypes.REQUEST.LOADING_SUCCESS:
      var successData = _.assign({
        actionType: type,
        actionStatus: actionTypes.REQUEST.LOADING_SUCCESS,
        responseError: undefined,
        responseStatus: action.responseStatus
      }, options.successData || {});

      return _.assign(_.cloneDeep(state), successData);
    case actionTypes.REQUEST.LOADING_ERROR:
      var errorData = _.assign({
        actionType: type,
        actionStatus: actionTypes.REQUEST.LOADING_ERROR,
        responseError: action.responseData,
        responseStatus: action.responseStatus
      }, options.errorData);

      return _.assign(_.cloneDeep(state), errorData);
    default:
      return state;
  }
}
