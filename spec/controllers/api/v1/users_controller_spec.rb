require 'rails_helper'

describe Api::V1::UsersController do
  let(:user) { FactoryGirl.create(:user) }
  let(:valid_params) {
    FactoryGirl.attributes_for(:user)
  }

  describe "POST create" do
    context "with invalid parameters" do
      attrs = FactoryGirl.attributes_for(:user).except(:password_confirmation)
      attrs.each do |field, value|
        it "does not create user or identity if #{field} is blank" do
          expect {
            post :create, params: {user: valid_params.except(field)}
          }.to change{User.count}.by(0)

          expect(json["errors"]).to be_a(Hash)
          expect(json["errors"][field.to_s]).to be_present
        end
      end

      it "does not create user if email is already in use" do
        user_params = valid_params.merge(email: user.email)
        expect {
          post :create, params: {user: user_params}
        }.to change{User.count}.by(0)

        expect(json["errors"]).to be_a(Hash)
        expect(json["errors"]["email"]).to be_present
      end
    end

    context "with valid parameters" do
      it "creates user" do
        user_params = valid_params
        expect {
          post :create, params: {user: user_params}
        }.to change{User.count}.by(1)

        created_user = User.order(created_at: :desc).first
        expect(created_user.email).to eql(user_params[:email])
        expect(created_user.first_name).to eql(user_params[:first_name])
        expect(created_user.last_name).to eql(user_params[:last_name])
      end
    end
  end
end
