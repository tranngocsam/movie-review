require 'rails_helper'

describe Api::V1::SessionsController do
  let(:user) { FactoryGirl.create(:user) }

  describe "GET me" do
    it "returns data for not logged in user" do
      get :me
      expect(response.status).to eql(200)
      expect(json["data"]).to be_blank
    end

    it "returns current user for logged in user" do
      login_user(user)
      get :me
      expect(response.status).to eql(200)

      session_json = json["data"]
      expect_simple_session_info(session_json, user)
    end
  end

  describe "POST create" do
    let(:valid_params) {
      {login: user.email, password: user.password}
    }

    it "returns error if email does not exist" do
      post :create, params: {user: valid_params.merge(login: Faker::Internet.email)}
      expect(response.status).to eql(422)
      expect(controller.current_user).to eql(nil)
    end

    it "returns error if password does not match" do
      post :create, params: {user: valid_params.merge(password: "not exist")}
      expect(response.status).to eql(422)
      expect(controller.current_user).to eql(nil)
    end

    it "allows user to login" do
      post :create, params: {user: valid_params}
      expect(response.status).to eql(200)
      expect(controller.current_user).to eql(user)
    end

    it "returns logged in user data" do
      post :create, params: {user: valid_params}
      expect(response.status).to eql(200)

      session_json = json["data"]
      expect_simple_session_info(session_json, user)
    end
  end

  describe "POST social_login" do
    let(:valid_params) {
      {"token" => SecureRandom.hex(16), "provider" => ["facebook", "google_login"].sample}
    }

    let(:profile) {
      {
        "id" => SecureRandom.hex(16),
        "first_name" => Faker::Name.first_name,
        "last_name" => Faker::Name.last_name,
        "email" => Faker::Internet.email
      }
    }

    context "with FB account" do
      it "creates user from FB info if email hasn't been used" do
        allow(FacebookService).to receive(:get_profile).and_return(profile)

        expect {
          post :social_login, params: {social: valid_params.merge("provider" => "facebook")}
        }.to change{User.count}.by(1)
        expect(response.status).to eql(200)
        expect(controller.current_user).to be_present

        user = User.find_by_email(profile["email"])
        expect(user).to be_present
      end

      it "logs user in from FB info if email has been used" do
        user = FactoryGirl.create(:user, email: profile["email"])
        allow(FacebookService).to receive(:get_profile).and_return(profile)

        expect {
          post :social_login, params: {social: valid_params.merge("provider" => "facebook")}
        }.to change{User.count}.by(0)
        expect(response.status).to eql(200)
        expect(controller.current_user).to eql(user)
      end

      it "returns logged in user data" do
        allow(FacebookService).to receive(:get_profile).and_return(profile)
        post :social_login, params: {social: valid_params.merge("provider" => "facebook")}
        expect(response.status).to eql(200)

        user = User.find_by_email(profile["email"])
        session_json = json["data"]
        expect_simple_session_info(session_json, user)
      end
    end

    context "with Google account" do
      it "creates user from Google info if email hasn't been used" do
        allow(GoogleService).to receive(:user_profile).and_return(profile)

        expect {
          post :social_login, params: {social: valid_params.merge("provider" => "google_login")}
        }.to change{User.count}.by(1)
        expect(response.status).to eql(200)
        expect(controller.current_user).to be_present

        user = User.find_by_email(profile["email"])
        expect(user).to be_present
      end

      it "logs user in from FB info if email has been used" do
        user = FactoryGirl.create(:user, email: profile["email"])
        allow(GoogleService).to receive(:user_profile).and_return(profile)

        expect {
          post :social_login, params: {social: valid_params.merge("provider" => "google_login")}
        }.to change{User.count}.by(0)
        expect(response.status).to eql(200)
        expect(controller.current_user).to eql(user)
      end

      it "returns logged in user data" do
        allow(GoogleService).to receive(:user_profile).and_return(profile)
        post :social_login, params: {social: valid_params.merge("provider" => "google_login")}
        expect(response.status).to eql(200)

        user = User.find_by_email(profile["email"])
        session_json = json["data"]
        expect_simple_session_info(session_json, user)
      end
    end
  end

  def expect_simple_session_info(session_json, user)
    expect(session_json["id"]).to eql(user.id)
    expect(session_json["email"]).to eql(user.email)
  end
end
