require 'rails_helper'

describe Api::V1::MoviesController do
  let(:user) { FactoryGirl.create(:user) }

  describe "GET index" do
    context "when user is not logged in" do
      it "requires user to login" do
        get :index, params: {format: :json}
        expect(response.status).to eql(401)
      end
    end

    context "when user already logged in" do
      before(:each) do
        login_user(user)
      end

      it "returns array of movies" do
        movies = FactoryGirl.create_list(:movie, 2)
        get :index

        expect(response).to be_ok
        expect(json["data"]).to be_a(Array)

        movies.each do |movie|
          movie_json = json["data"].find{|js| js["id"] == movie.id}
          expect(movie_json).to be_present
          expect(movie_json["title"]).to be_present
        end
      end

      it "paginates movies" do
        movies = FactoryGirl.create_list(:movie, 6)

        page = 2
        per_page = 2
        get :index, params: {page: page, per_page: per_page}
        expect(response).to be_ok

        expect(json["data"]).to be_a(Array)
        expect(json["data"].length).to eql(per_page)
        expect(json["pagination"]).to be_a(Hash)
        expect(json["pagination"]["current_page"]).to eql(page)
        expect(json["pagination"]["count"]).to eql(movies.length)
      end
    end
  end
end
