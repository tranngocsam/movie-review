FactoryGirl.define do
  factory :user do
    first_name { Faker::Name.first_name }
    last_name { Faker::Name.last_name }
    sequence(:email) { |n| "leopold#{n}@habsburgfamily.es" }

    password "password"
    password_confirmation "password"
  end
end
