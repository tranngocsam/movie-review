FactoryGirl.define do
  factory :movie do
    title { Faker::Book.name }
    released_date { 2.years.ago }
  end
end
