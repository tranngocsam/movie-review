class CreateMovies < ActiveRecord::Migration[5.1]
  def change
    create_table :movies do |t|
      t.string :title, limit: 255
      t.string :studio, limit: 255
      t.date :released_date
      t.integer :status
      t.string :sound, limit: 10
      t.string :version, limit: 50
      t.decimal :price, precision: 10, scale: 2
      t.string :rating, limit: 10
      t.string :year, limit: 10
      t.string :genre, limit: 50
      t.string :aspect, limit: 20
      t.string :upc, limit: 20
      t.date :dvd_release_date
      t.integer :external_id
      t.date :timestamp
      t.boolean :updated

      t.timestamps
    end
  end
end
