class CreateSocialConnections < ActiveRecord::Migration[5.1]
  def change
    create_table :social_connections do |t|
      t.string    :provider, limit: 50
      t.string    :uid, limit: 50
      t.json      :credentials
      t.json      :raw
      t.json      :external_info
      t.datetime  :deleted_at

      t.references      :social_connectable, polymorphic: true, index: {name: "index_social_connections_on_social_connectable"}

      t.timestamps
    end
  end
end
