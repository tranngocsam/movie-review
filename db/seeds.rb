# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

require 'csv'

map = {
  0 => :title,
  1 => :studio,
  2 => :released_date,
  3 => :status,
  4 => :sound,
  5 => :version,
  6 => :price,
  7 => :rating,
  8 => :year,
  9 => :genre,
  10 => :aspect,
  11 => :upc,
  12 => :dvd_release_date,
  13 => :external_id,
  14 => :timestamp,
  15 => :updated
}

file_path = "#{Rails.root.to_s}/db/recent_movies.csv"
i = 0
CSV.foreach(file_path) do |row|
  i += 1
  next if i == 1

  attrs = {}
  row.each_with_index do |value, index|
    attrs[map[index]] = value
  end

  attrs[:released_date] = Date.strptime(attrs[:released_date], "%m/%d/%y") if attrs[:released_date].present?
  attrs[:dvd_release_date] = Date.strptime(attrs[:dvd_release_date], "%m/%d/%y") if attrs[:dvd_release_date].present?
  attrs[:timestamp] = Date.strptime(attrs[:timestamp], "%m/%d/%y") if attrs[:timestamp].present?
  attrs[:price] = attrs[:price].slice(1, attrs[:price].length) if attrs[:price].present?

  Movie.find_or_create_by!(attrs)
  puts "Movie at row #{i - 1} was upserted"
end
